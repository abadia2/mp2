import enum
import logging
import os
import socket
import threading
from multiprocessing import Value
from typing import Tuple

from helper import LOGGER_ADMIN_WORKER, pretty_local_membership_list
from type_hints import LocalMembershipList

_ADMIN_WORKER = "ADMIN WORKER"
_ADMIN_PORT = 33334
_MESSAGE_SIZE = 1

logger = logging.getLogger(LOGGER_ADMIN_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("logs/admin_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


class MessageType(enum.Enum):
    LIST_MEM = 1
    LIST_SELF = 2
    JOIN = 3
    LEAVE = 4
    ENABLE_SUSPICION = 5
    DISABLE_SUSPICION = 6
    DISCONNECT = 7


def message_type_to_bytes(_type: MessageType) -> bytes:
    return _type.value.to_bytes(length=1, byteorder='big')


def message_type_from_bytes(_bytes: bytes) -> int:
    return int.from_bytes(_bytes, byteorder='big')


def read_socket(server: socket, buffer: bytes) -> Tuple[bytes, bytes]:
    while len(buffer) < _MESSAGE_SIZE:
        _bytes: bytes = server.recv(_MESSAGE_SIZE)
        buffer += _bytes
    byte_msg: bytes = buffer[:_MESSAGE_SIZE]
    _buffer_overflow: bytes = buffer[_MESSAGE_SIZE:]
    return byte_msg, _buffer_overflow


def handle_admin_message(
        host: str, connection: socket,
        local_membership_list: LocalMembershipList,
        suspicion_flag: Value, send_worker_flag: Value
) -> None:
    log_src = f"[{host} - {threading.currentThread().name} - {_ADMIN_WORKER}]"
    initial_buffer = b''
    try:
        while True:
            _byte, _buffer_overflow = read_socket(connection, initial_buffer)
            initial_buffer = _buffer_overflow
            message_type = message_type_from_bytes(_byte)
            logger.info(f"{log_src} Received admin command: {MessageType(message_type).name}")
            if message_type == MessageType.LIST_MEM.value:
                logger.info(f"{log_src} LIST_MEM:\n{pretty_local_membership_list(local_membership_list)}")
                break
            elif message_type == MessageType.LIST_SELF.value:
                for node_id, (member, entry_time, is_sus) in local_membership_list.items():
                    if member.address == host:
                        logger.info(f"{log_src} LIST SELF: {node_id}")
                break
            elif message_type == MessageType.JOIN.value:
                with send_worker_flag.get_lock():
                    send_worker_flag.value = 1
                logger.info(f"{log_src} Raised send_worker_flag={send_worker_flag.value}")
                break
            elif message_type == MessageType.LEAVE.value:
                with send_worker_flag.get_lock():
                    send_worker_flag.value = 0
                logger.info(f"{log_src} Lowered send_worker_flag={send_worker_flag.value}")
                break
            elif message_type == MessageType.ENABLE_SUSPICION.value:
                with suspicion_flag.get_lock():
                    suspicion_flag.value = 1
                logger.info(f"{log_src} Raised suspicion_flag={suspicion_flag.value}")
                break
            elif message_type == MessageType.DISABLE_SUSPICION.value:
                with suspicion_flag.get_lock():
                    suspicion_flag.value = 0
                logger.info(f"{log_src} Lowered suspicion_flag={suspicion_flag.value}")
                break
            elif message_type == MessageType.DISCONNECT:
                break
    except Exception as ex:
        logger.error(f"{log_src} {ex}")
    finally:
        connection.close()


def admin_worker(
        host: str, local_membership_list: LocalMembershipList, suspicion_flag: Value, send_worker_flag: Value
) -> None:
    log_src = f"[{host} - {os.getpid()} - {_ADMIN_WORKER}]"
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, _ADMIN_PORT))
    logger.debug(f"{log_src} STARTED")
    s.listen()
    logger.debug(f"{log_src} Listening on port {_ADMIN_PORT}...")
    try:
        while True:
            connection, address = s.accept()
            thread = threading.Thread(
                target=handle_admin_message,
                args=(host, connection, local_membership_list, suspicion_flag, send_worker_flag)
            )
            thread.start()
    except Exception as e:
        logger.error(e)
    finally:
        s.close()
