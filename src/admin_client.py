import socket
from multiprocessing import Process

from admin_worker import MessageType, _ADMIN_PORT

_TARGETS = [
    "fa23-cs425-7801.cs.illinois.edu",
    "fa23-cs425-7802.cs.illinois.edu",
    "fa23-cs425-7803.cs.illinois.edu",
    "fa23-cs425-7804.cs.illinois.edu",
    "fa23-cs425-7805.cs.illinois.edu",
    "fa23-cs425-7806.cs.illinois.edu",
    "fa23-cs425-7807.cs.illinois.edu",
    "fa23-cs425-7808.cs.illinois.edu",
    "fa23-cs425-7809.cs.illinois.edu",
    "fa23-cs425-7810.cs.illinois.edu"
]
TIMEOUT = 3


def handle_command(target: str, message_type: MessageType) -> None:
    target_ip = socket.gethostbyname(target)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(TIMEOUT)
    sock.connect((target_ip, _ADMIN_PORT))
    sock.send(message_type.value.to_bytes(1, byteorder='big'))
    sock.close()


def execute_command(message_type: MessageType) -> None:
    target_vms = input(
        f"List the virtual machines are you targeting with commas in between (enter for all): "
    ).lower()
    if target_vms == "":
        target_vms = _TARGETS
    else:
        target_vms = [target.strip() for target in target_vms.split(",")]
    print(target_vms)
    confirm = input(
        f"Which machines are you targeting '{str(message_type).replace('MessageType.', '')}'? (yes/no): "
    ).lower()
    if confirm == "yes":
        processes = [Process(target=handle_command, args=(target, message_type)) for target in target_vms]
        for process in processes:
            process.start()
        for process in processes:
            process.join()


def main_menu() -> None:
    while True:
        print("\nMain Menu:")
        for message_type in MessageType:
            print(f"\t{message_type.value}. {str(message_type).replace('MessageType.', '')}")
        choice = input("Enter your choice ('quit' to exit menu): ")
        if choice == 'quit':
            exit()
        for message_type in MessageType:
            if str(message_type.value) == choice:
                execute_command(message_type)


if __name__ == "__main__":
    main_menu()
