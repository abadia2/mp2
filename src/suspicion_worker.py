import logging
import math
import os
from datetime import datetime
from multiprocessing import Queue, Value
from threading import Thread, Lock
from typing import Dict

import message_pb2
from expiration_worker import expiration_worker
from helper import LOGGER_SUSPICION_WORKER, pretty_local_membership_list
from type_hints import LocalMembershipList

logger = logging.getLogger(LOGGER_SUSPICION_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(f"logs/suspicion_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

file_handler = logging.FileHandler(f"logs/main.log")
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def suspicion_worker(
        host: str,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue,
        suspicion_flag: Value, fail_worker_flag: Value, suspicion_worker_flag: Value
) -> None:
    log_src = f"[{host} - {os.getpid()} - SUSPICION WORKER]"
    lock = Lock()
    while True:

        # Remember node's that are in the process of being deleted
        cache: Dict[str, Thread] = {}

        # Tell fail worker to go idle
        if suspicion_flag.value == 1 and suspicion_worker_flag.value == 1 and fail_worker_flag.value == 1:
            with fail_worker_flag.get_lock():
                fail_worker_flag.value = 0

        # Continuously check for nodes that should be marked as suspicious
        while suspicion_flag.value == 1 and suspicion_worker_flag.value == 1:
            for node_id, (member, entry_time, is_suspicious) in local_membership_list.items():
                if member.address == host or cache.get(node_id) is not None:
                    # Don't remove myself from local_membership_list or resend a node that is already in cleanup
                    continue
                now = datetime.now()
                t_fail = math.log(len(local_membership_list))
                if (now - entry_time).total_seconds() > t_fail:
                    is_suspicious = True
                    local_membership_list.update({node_id: (member, entry_time, True)})
                    logger.warning(f"{log_src} {node_id} is suspicious: Now[{now}] - Update_Time[{entry_time}] > t_fail[{t_fail}]")
                    logger.debug(pretty_local_membership_list(local_membership_list))
                if is_suspicious:
                    suspicious_message = message_pb2.SuspicionMessage()
                    suspicious_message.type = message_pb2.SuspicionType.SUSPICIOUS
                    suspicious_message.suspect.CopyFrom(member)
                    suspicion_messages_queue.put(suspicious_message)
                    t = Thread(
                        target=expiration_worker,
                        args=(
                            host,
                            t_fail * 2, member,
                            local_membership_list, suspicion_messages_queue, cache, lock,
                            suspicion_flag
                        )
                    )
                    t.start()
                    with lock:
                        cache.update({node_id: t})
                    logger.debug(f"{log_src} Sent {node_id} to EXPIRATION WORKER: is_suspicious[{is_suspicious}]")
        for node_id, thread in cache.items():
            # Wait for all children to finish gracefully
            thread.join()
        with fail_worker_flag.get_lock():
            # Allow Fail Worker to start after all children finish gracefully
            fail_worker_flag.value = 1
