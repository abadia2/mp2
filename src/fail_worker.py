import logging
import math
import os
import socket
from datetime import datetime
from multiprocessing import Value
from threading import Lock, Thread
from typing import Dict

from cleanup_worker import cleanup_worker
from helper import INTRODUCER, LOGGER_FAIL_WORKER
from type_hints import LocalMembershipList

_FAIL_WORKER = "FAIL WORKER"

logger = logging.getLogger(LOGGER_FAIL_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("logs/fail_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def fail_worker(
        host: str,
        local_membership_list: LocalMembershipList,
        suspicion_flag: Value, fail_worker_flag: Value, suspicion_worker_flag: Value
) -> None:
    log_source = f"{host} - {os.getpid()} - {_FAIL_WORKER}"
    introducer_address = socket.gethostbyname(INTRODUCER)
    lock = Lock()
    while True:
        cache: Dict[str, Thread] = {}
        if suspicion_flag.value == 0 and fail_worker_flag.value == 1 and suspicion_worker_flag.value == 1:
            with suspicion_worker_flag.get_lock():
                # Tell suspicion worker to idle
                suspicion_worker_flag.value = 0
        while suspicion_flag.value == 0 and fail_worker_flag.value == 1:
            for node_id, (member, entry_time, is_failed) in local_membership_list.items():
                if member.address == host or member.address == introducer_address or cache.get(node_id) is not None:
                    continue
                now = datetime.now()
                t_fail = math.log(len(local_membership_list))
                if (now - entry_time).total_seconds() > t_fail or is_failed:
                    local_membership_list.update({node_id: (member, entry_time, True)})
                    t = Thread(
                        target=cleanup_worker,
                        args=(
                            host,
                            t_fail * 2, node_id,
                            local_membership_list,
                            cache, lock
                        )
                    )
                    t.start()
                    with lock:
                        cache.update({node_id: t})
                    logger.warning(f"[{log_source} - {datetime.now()}] Failure detected: {node_id}")
                    logger.debug(f"Now[{now}] - Entry_Time[{entry_time}] > t_fail[{t_fail}]")
        for node_id, thread in cache.items():
            # Wait for all children to finish gracefully
            thread.join()
        with suspicion_worker_flag.get_lock():
            # Allow Suspicion Worker to start after all children finish gracefully
            suspicion_worker_flag.value = 1
