# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: message.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\rmessage.proto\"v\n\x06Member\x12\x0f\n\x07\x61\x64\x64ress\x18\x01 \x01(\t\x12\x0c\n\x04port\x18\x02 \x01(\x05\x12\x16\n\x0eversion_number\x18\x03 \x01(\x03\x12\x19\n\x11heartbeat_counter\x18\x04 \x01(\x03\x12\x1a\n\x12incarnation_number\x18\x05 \x01(\x03\"*\n\x0eMembershipList\x12\x18\n\x07members\x18\x01 \x03(\x0b\x32\x07.Member\"J\n\x10SuspicionMessage\x12\x1c\n\x04type\x18\x01 \x01(\x0e\x32\x0e.SuspicionType\x12\x18\n\x07suspect\x18\x02 \x01(\x0b\x32\x07.Member\"F\n\x15SuspicionMessagesList\x12-\n\x12suspicion_messages\x18\x01 \x03(\x0b\x32\x11.SuspicionMessage\"n\n\tHeartbeat\x12(\n\x0fmembership_list\x18\x01 \x01(\x0b\x32\x0f.MembershipList\x12\x37\n\x17suspicion_messages_list\x18\x02 \x01(\x0b\x32\x16.SuspicionMessagesList*7\n\rSuspicionType\x12\t\n\x05\x41LIVE\x10\x00\x12\x0e\n\nSUSPICIOUS\x10\x01\x12\x0b\n\x07\x43ONFIRM\x10\x02\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'message_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  _globals['_SUSPICIONTYPE']._serialized_start=441
  _globals['_SUSPICIONTYPE']._serialized_end=496
  _globals['_MEMBER']._serialized_start=17
  _globals['_MEMBER']._serialized_end=135
  _globals['_MEMBERSHIPLIST']._serialized_start=137
  _globals['_MEMBERSHIPLIST']._serialized_end=179
  _globals['_SUSPICIONMESSAGE']._serialized_start=181
  _globals['_SUSPICIONMESSAGE']._serialized_end=255
  _globals['_SUSPICIONMESSAGESLIST']._serialized_start=257
  _globals['_SUSPICIONMESSAGESLIST']._serialized_end=327
  _globals['_HEARTBEAT']._serialized_start=329
  _globals['_HEARTBEAT']._serialized_end=439
# @@protoc_insertion_point(module_scope)
