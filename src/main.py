import argparse
import logging
import os
import socket
from ctypes import c_int
from datetime import datetime
from multiprocessing import Process, Manager, Queue, Value

import message_pb2
from admin_worker import admin_worker
from fail_worker import fail_worker
from send_worker import send_worker
from helper import get_node_id, INTRODUCER, pretty_local_membership_list, APP_NAME
from receive_worker import receive_worker
from suspicion_worker import suspicion_worker
from type_hints import LocalMembershipList


_PORT = 420_96
_SERVER = "SERVER"
_SUSPICION_ENABLED = False
_T_GOSSIP = .25

logger = logging.getLogger(APP_NAME)
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler("logs/main.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def load_introducer(port: int, local_membership_list: LocalMembershipList) -> None:
    membership_list = message_pb2.MembershipList()

    # Every virtual machine will know the introducer's address before gossiping starts
    introducer_address = socket.gethostbyname(INTRODUCER)
    introducer = message_pb2.Member()
    introducer.address = introducer_address
    introducer.port = port
    introducer.version_number = 0
    introducer.heartbeat_counter = 0
    introducer.incarnation_number = 0
    membership_list.members.append(introducer)

    # Every virtual machine knows its own address before gossiping starts, but don't add the introducer twice!
    my_address = socket.gethostbyname(socket.gethostname())
    if introducer_address != my_address:
        me = message_pb2.Member()
        me.address = my_address
        me.port = port
        me.version_number = int(round(datetime.now().timestamp()))
        me.heartbeat_counter = 0
        me.incarnation_number = 0
        membership_list.members.append(me)

    for member in membership_list.members:
        local_membership_list.update({get_node_id(member): (member, datetime.now(), False)})


def initialize(host: str, port: int, t_gossip: int, enable_suspicion: bool) -> None:
    log_src = f"[{host} - {os.getpid()} - {_SERVER}]"
    logger.info(f"{log_src} Initializing shared memory...")

    with Manager() as manager:

        # Shared memory for all parallel processes
        local_membership_list: LocalMembershipList = manager.dict()
        suspicion_messages_queue: Queue = Queue()

        # Start with suspicion disabled by default
        send_worker_flag = Value(c_int, 1)

        if enable_suspicion:
            suspicion_flag = Value(c_int, 1)
            fail_worker_flag = Value(c_int, 0)
            suspicion_worker_flag = Value(c_int, 1)
        else:
            suspicion_flag = Value(c_int, 0)
            fail_worker_flag = Value(c_int, 1)
            suspicion_worker_flag = Value(c_int, 0)

        logger.info(f"{log_src} Initializing processes...")

        load_introducer(port, local_membership_list)
        logger.info(f"{log_src} INITIALIZED Local Membership List:\n{pretty_local_membership_list(local_membership_list)}")

        (
            Process(
                target=send_worker,
                args=(
                    host,
                    t_gossip,
                    local_membership_list, suspicion_messages_queue,
                    suspicion_flag, send_worker_flag
                )
            )
        ).start()
        logger.info(f"{log_src} STARTED Send Worker with t_gossip={t_gossip}")

        (
            Process(
                target=suspicion_worker,
                args=(
                    host,
                    local_membership_list, suspicion_messages_queue,
                    suspicion_flag, fail_worker_flag, suspicion_worker_flag
                )
            )
        ).start()
        logger.info(f"{log_src} STARTED Suspicion Worker")

        (
            Process(
                target=fail_worker,
                args=(
                    host,
                    local_membership_list,
                    suspicion_flag, fail_worker_flag, suspicion_worker_flag
                )
            )
        ).start()
        logger.info(f"{log_src} STARTED Fail Worker")

        (
            Process(
                target=receive_worker,
                args=(
                    host, port,
                    local_membership_list, suspicion_messages_queue,
                    suspicion_flag
                )
            )
        ).start()
        logger.info(f"{log_src} STARTED Receive Worker with suspicion_flag={suspicion_flag.value} and send_worker_flag={send_worker_flag.value}")

        (
            Process(
                target=admin_worker,
                args=(host, local_membership_list, suspicion_flag, send_worker_flag)
            )
        ).start()
        logger.info(f"{log_src} STARTED Admin Worker with suspicion_flag={suspicion_flag.value} and send_worker_flag={send_worker_flag.value}")

        while True:
            # Let server processes run
            pass


def main() -> None:
    parser = argparse.ArgumentParser(description="Distributed Group Membership Server")
    parser.add_argument("--port", type=int, help=f"Default is {_PORT}.")
    parser.add_argument("--t_gossip", type=int, help=f"Default is {_T_GOSSIP}.")
    parser.add_argument("--suspicion", action="store_true", help=f"Default is {_SUSPICION_ENABLED}")
    args = parser.parse_args()
    port = args.port or _PORT
    t_gossip = args.t_gossip or _T_GOSSIP
    enable_suspicion = args.suspicion or False
    host = socket.gethostbyname(socket.gethostname())
    initialize(host, port, t_gossip, enable_suspicion)


if __name__ == "__main__":
    main()
