import logging
import os
from datetime import datetime
from multiprocessing import Queue, Value, Process
from threading import Lock
from typing import Dict

import message_pb2
from helper import get_node_id, LOGGER_EXPIRATION_WORKER
from type_hints import LocalMembershipEntry, LocalMembershipList

logger = logging.getLogger(LOGGER_EXPIRATION_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("logs/expiration_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

file_handler = logging.FileHandler("logs/main.log")
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def expiration_worker(
        host: str,
        t_expire: int, target: message_pb2.Member,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue, cache: Dict[str, Process], lock: Lock,
        suspicion_flag: Value
) -> None:
    """ Wait t_expire seconds before removing the member from the membership list """
    log_src = f"{host} - {os.getpid()} - EXPIRATION WORKER"
    node_id = get_node_id(target)
    start = datetime.now()
    while suspicion_flag.value == 1:
        entry: LocalMembershipEntry = local_membership_list.get(node_id)
        if entry is None:
            with lock:
                cache.pop(node_id)
            logger.warning(f"{log_src} {node_id} was already deleted")
            return
        member, entry_time, is_suspicious = entry
        if not is_suspicious:
            alive_message = message_pb2.SuspicionMessage()
            alive_message.type = message_pb2.SuspicionType.ALIVE
            alive_message.suspect.CopyFrom(member)
            suspicion_messages_queue.put(alive_message)
            with lock:
                cache.pop(node_id)
            logger.warning(f"{log_src} {node_id} is no longer suspicious")
            return
        now = datetime.now()
        if (now - start).total_seconds() > t_expire:
            local_membership_list.pop(node_id)
            confirm_message = message_pb2.SuspicionMessage()
            confirm_message.type = message_pb2.SuspicionType.CONFIRM
            confirm_message.suspect.CopyFrom(member)
            suspicion_messages_queue.put(confirm_message)
            with lock:
                cache.pop(node_id)
            logger.warning(f"[{log_src} - {datetime.now()}] Deleted {node_id}: Now[{now}] - Entry_Time{entry_time} > {t_expire}")
            return
