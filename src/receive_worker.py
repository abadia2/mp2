import logging
import os
import socket
import threading
from datetime import datetime
from multiprocessing import Queue, Value

import message_pb2
from helper import get_node_id, LOGGER_RECEIVE_WORKER, pretty_local_membership_list
from type_hints import LocalMembershipEntry, LocalMembershipList

_MESSAGE_SIZE = 1_326
_RECEIVE_WORKER = "RECEIVE WORKER"

logger = logging.getLogger(LOGGER_RECEIVE_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(f"logs/receive_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

main_file_handler = logging.FileHandler(f"logs/main.log")
main_file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def merge_membership_list(
        host: str,
        local_membership_list: LocalMembershipList, foreign_membership_list: message_pb2.MembershipList
) -> None:
    """ Merge local membership lists received from other virtual machines """
    log_source = f"[{host} - {threading.currentThread().name} - {_RECEIVE_WORKER} - {datetime.now()}]"

    for foreign_member in foreign_membership_list.members:
        node_id = get_node_id(foreign_member)

        # If not already present in the local membership list, add foreign member in the local membership list
        entry: LocalMembershipEntry = local_membership_list.get(node_id)
        if entry is None:
            new_member = {node_id: (foreign_member, datetime.now(), False)}
            local_membership_list.update(new_member)
            logger.debug(f"{log_source} Added foreign member to membership list")
            logger.debug(pretty_local_membership_list(local_membership_list))
            continue

        # Merger heartbeat counters
        local_member, entry_time, is_suspicious = entry
        if local_member.heartbeat_counter < foreign_member.heartbeat_counter:
            local_member.heartbeat_counter = foreign_member.heartbeat_counter
            merged_member = {node_id: (local_member, datetime.now(), False)}
            local_membership_list.update(merged_member)
            logger.debug(f"{log_source} Merged heartbeat counter")
            logger.debug(pretty_local_membership_list(local_membership_list))
            continue


def handle_suspicion_message(
        host: str,
        suspicion_message: message_pb2.SuspicionMessage, local_membership_list: LocalMembershipList,
        suspicion_messages_queue: Queue
) -> None:
    """ If suspicion is enabled, handle the "suspicious" message received from other virtual machines """
    log_source = f"[{host} - {threading.currentThread().name} - {_RECEIVE_WORKER} - {datetime.now()}]"
    node_id = get_node_id(suspicion_message.suspect)

    entry: LocalMembershipEntry = local_membership_list.get(node_id)
    if entry is None:
        # Virtual machine that is "suspicious" was already removed from the local membership list
        return

    local_member, entry_time, is_suspicious = entry
    if local_member.incarnation_number > suspicion_message.suspect.incarnation_number:
        # Ignore lower incarnation numbers, as described in the SWIM research paper
        return

    if suspicion_message.suspect.address == host:
        # Based on the SWIM research paper, only this virtual machine can increment the incarnation number for itself
        local_member.incarnation_number += 1

        # Queue the "alive" message, so it is piggybacked on the next heartbeat
        alive_message = message_pb2.SuspicionMessage()
        alive_message.type = message_pb2.SuspicionType.ALIVE
        alive_message.suspect.CopyFrom(local_member)
        suspicion_messages_queue.put(alive_message)
    elif not is_suspicious:
        # Mark member as suspicious
        local_membership_list.update({node_id: (local_member, entry_time, True)})
        logger.warning(f"{log_source} SUSPICIOUS message received, marked {node_id} as suspicious")
        logger.debug(pretty_local_membership_list(local_membership_list))

        # Queue the "alive" message, so it is piggybacked on the next heartbeat
        suspicious_message = message_pb2.SuspicionMessage()
        suspicious_message.type = message_pb2.SuspicionType.SUSPICIOUS
        suspicious_message.suspect.CopyFrom(local_member)
        suspicion_messages_queue.put(suspicious_message)


def handle_alive_message(
        host: str,
        suspicion_message: message_pb2.SuspicionMessage,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue
) -> None:
    """ If suspicion is enabled, handle the "alive" message received from other virtual machines """
    log_source = f"[{host} - {threading.currentThread().name} - {_RECEIVE_WORKER} - {datetime.now()}]"
    node_id = get_node_id(suspicion_message.suspect)

    entry: LocalMembershipEntry = local_membership_list.get(node_id)
    if entry is None:
        # TODO: Check Gossip paper and SWIM paper for how to handle this case?
        return

    local_member, entry_time, is_suspicious = entry
    if local_member.incarnation_number > suspicion_message.suspect.incarnation_number:
        # Ignore lower incarnation numbers, as described in the SWIM research paper
        return

    if is_suspicious:
        # Mark the member as no longer suspicious
        local_membership_list.update({node_id: (local_member, entry_time, False)})
        logger.warning(f"{log_source} ALIVE message received, {node_id} is no longer marked as suspicious")
        logger.debug(pretty_local_membership_list(local_membership_list))

        # Queue the "alive" message, so it is piggybacked on the next heartbeat
        alive_message = message_pb2.SuspicionMessage()
        alive_message.type = message_pb2.SuspicionType.ALIVE
        alive_message.suspect.CopyFrom(local_member)
        suspicion_messages_queue.put(alive_message)


def handle_confirm_message(
        host: str,
        suspicion_message: message_pb2.SuspicionMessage,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue
) -> None:
    """ If suspicion is enabled, handle the "confirm" message received from other virtual machines """
    log_source = f"[{host} - {threading.currentThread().name} - {_RECEIVE_WORKER} - {datetime.now()}]"
    node_id = get_node_id(suspicion_message.suspect)

    entry: LocalMembershipEntry = local_membership_list.get(node_id)
    if entry is None:
        # Virtual machine that is "confirmed" as failed was already removed from the local membership list
        return

    local_member, entry_time, is_suspicious = entry
    if local_member.incarnation_number > suspicion_message.suspect.incarnation_number:
        # Ignore lower incarnation numbers, as described in the SWIM research paper
        return

    # Remove from local membership list
    local_membership_list.pop(node_id)
    logger.info(f"{log_source} CONFIRM message received, {node_id} was removed")
    logger.debug(pretty_local_membership_list(local_membership_list))

    # Queue the "confirm" message, so it is piggybacked on the next heartbeat
    confirmation_message = message_pb2.SuspicionMessage()
    confirmation_message.type = message_pb2.SuspicionType.CONFIRM
    confirmation_message.suspect.CopyFrom(local_member)
    suspicion_messages_queue.put(confirmation_message)


def receive_heartbeat(
        host: str, byte_heartbeat: bytes,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue,
        suspicion_flag: Value
) -> None:
    """ Handle a heartbeat received from other virtual machines """
    try:
        # Parse bytes to a protobuf Heartbeat Object (see src/message.proto)
        heartbeat = message_pb2.Heartbeat()
        heartbeat.ParseFromString(byte_heartbeat)

        # Merge foreign membership lists into this virtual machines local membership list
        merge_membership_list(host, local_membership_list, heartbeat.membership_list)

        # Only if suspicion is enabled, handle the suspicion messages piggybacked on the heartbeat
        if suspicion_flag.value == 1:
            for suspicion_message in heartbeat.suspicion_messages_list.suspicion_messages:
                if suspicion_message.type == message_pb2.SuspicionType.SUSPICIOUS:
                    handle_suspicion_message(host, suspicion_message, local_membership_list, suspicion_messages_queue)
                elif suspicion_message.type == message_pb2.SuspicionType.ALIVE:
                    handle_alive_message(host, suspicion_message, local_membership_list, suspicion_messages_queue)
                elif suspicion_message.type == message_pb2.SuspicionType.CONFIRM:
                    handle_confirm_message(host, suspicion_message, local_membership_list, suspicion_messages_queue)
    except Exception as ex:
        logger.error(ex)


def receive_worker(
        host: str, port: int,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue,
        suspicion_flag: Value
) -> None:
    """ Start a socket server that spawns a thread per new connection """
    log_src = f"[{host} - {os.getpid()} - {_RECEIVE_WORKER}]"
    server = None
    try:
        logger.debug(f"{log_src} Initializing server...")
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.bind((host, port))
        logger.debug(f"{log_src} Server is listening...")
        while True:
            _bytes, _address = server.recvfrom(_MESSAGE_SIZE)
            (
                threading.Thread(
                    target=receive_heartbeat,
                    args=(host, _bytes, local_membership_list, suspicion_messages_queue, suspicion_flag)
                )
            ).start()
    except KeyboardInterrupt:
        logger.warning("Manually shut down socket server")
    except Exception as ex:
        logger.error(ex)
    finally:
        if server is not None:
            server.close()
