import logging
import os
import time
from datetime import datetime
from multiprocessing import Process
from threading import Lock
from typing import Dict

from helper import LOGGER_CLEANUP_WORKER
from type_hints import LocalMembershipEntry, LocalMembershipList

logger = logging.getLogger(LOGGER_CLEANUP_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("logs/cleanup_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

file_handler = logging.FileHandler("logs/main.log")
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)


def cleanup_worker(
        host: str,
        t_cleanup: int, node_id: str,
        local_membership_list: LocalMembershipList,
        cache: Dict[str, Process], lock: Lock
) -> None:
    log_src = f"{host} - {os.getpid()} CLEANUP WORKER"
    time.sleep(t_cleanup)
    entry: LocalMembershipEntry = local_membership_list.get(node_id)
    if entry is None:
        with lock:
            cache.pop(node_id)
        logger.warning(f"[{log_src} - {datetime.now()}] {node_id} was already deleted")
    local_membership_list.pop(node_id)
    with lock:
        cache.pop(node_id)
    logger.warning(f"[{log_src} - {datetime.now()}] Deleted {node_id} after t_cleanup[{t_cleanup}]")
