import json
from datetime import datetime

import message_pb2
from type_hints import LocalMembershipList

INTRODUCER = "fa23-cs425-7801.cs.illinois.edu"

# Loggers
_NOW = datetime.now().strftime('%Y-%m-%d-%H.%M.%S')
APP_NAME = "Distributed Failure Detection"
LOGGER_ADMIN_WORKER = f"{APP_NAME} Admin Worker"
LOGGER_CLEANUP_WORKER = f"{APP_NAME} Cleanup Worker"
LOGGER_EXPIRATION_WORKER = f"{APP_NAME} Expiration Worker"
LOGGER_FAIL_WORKER = f"{APP_NAME} Fail Worker"
LOGGER_RECEIVE_WORKER = f"{APP_NAME} Receive Worker"
LOGGER_SEND_WORKER = f"{APP_NAME} Send Worker"
LOGGER_SUSPICION_WORKER = f"{APP_NAME} Suspicion Worker"
LOGGER_SUFFIX = f"{_NOW}"


def get_node_id(member: message_pb2.Member) -> str:
    return f"{member.address}:{member.port}:{member.version_number}"


def pretty_local_membership_list(local_membership_list: LocalMembershipList) -> str:
    pretty_dict = {}
    for node_id, (member, entry_time, is_sus) in local_membership_list.items():
        pretty_dict.update({node_id: str(member).replace("\"", "").replace("\n", "    ")})
    return json.dumps(pretty_dict, indent=2, ensure_ascii=True)
