import logging
import math
import os
import random
import socket
import threading
import time
from datetime import datetime
from multiprocessing import Process, Queue, Value

from google.protobuf import text_format

import message_pb2
from helper import LOGGER_SEND_WORKER, pretty_local_membership_list
from type_hints import LocalMembershipList
import sys
_DROP_RATE = 0
_SEND_WORKER = "SEND WORKER"

logger = logging.getLogger(LOGGER_SEND_WORKER)
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(f"logs/send_worker.log")
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

file_handler = logging.FileHandler(f"logs/main.log")
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)


raw_byte_logger = logging.getLogger("Raw Gossip Bytes Logger")
raw_byte_logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(f"logs/raw-gossip-bytes.log")
file_handler.setLevel(logging.DEBUG)
raw_byte_logger.addHandler(file_handler)


sus_byte_logger = logging.getLogger("Suspicion Gossip Bytes Logger")
sus_byte_logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(f"logs/suspicion-gossip-bytes.log")
file_handler.setLevel(logging.DEBUG)
sus_byte_logger.addHandler(file_handler)


bandwidth_file_handler = logging.FileHandler(f"logs/bandwidth.log")
bandwidth_logger = logging.getLogger('BANDWIDTH')
bandwidth_logger.setLevel(logging.INFO)
bandwidth_logger.addHandler(bandwidth_file_handler)


def send_heartbeat(
        server: socket, host: str,
        target: message_pb2.Member,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue,
        suspicion_flag: Value
) -> None:
    """ Send heartbeat to randomly select member from the local membership list """
    log_source = f"[{host} - {os.getpid()} - {_SEND_WORKER}]"
    heartbeat = message_pb2.Heartbeat()
    for node_id, (member, entry_time, is_fail_or_sus) in local_membership_list.items():
        if not is_fail_or_sus:
            heartbeat.membership_list.members.append(member)
    if suspicion_flag.value == 1:
        count = 0
        while not suspicion_messages_queue.empty() and count < 10:
            suspicion_message: message_pb2.SuspicionMessage = suspicion_messages_queue.get()
            heartbeat.suspicion_messages_list.suspicion_messages.append(suspicion_message)
    logger.debug(f"{log_source} Sent heartbeat to {target.address}:\n{text_format.MessageToString(heartbeat, as_utf8=True, indent=2)}")

    byte_msg = heartbeat.SerializeToString()
    if suspicion_flag.value == 1:
        sus_byte_logger.debug(f"{datetime.now().timestamp()}, {len(byte_msg)}")
    else:
        raw_byte_logger.debug(f"{datetime.now().timestamp()}, {len(byte_msg)}")
    server.sendto(byte_msg, (target.address, target.port))

    bandwidth_logger.info(f"{datetime.now()} Heartbeat sent out of size {sys.getsizeof(heartbeat)}")
    server.sendto(byte_msg, (target.address, target.port))


def send_worker(
        host: str,
        t_gossip: int,
        local_membership_list: LocalMembershipList, suspicion_messages_queue: Queue,
        suspicion_flag: Value, send_worker_flag: Value
) -> None:
    """ Continuously send heartbeats in a given interval to n random targets from the local membership list """
    log_src = f"[{host} - {threading.currentThread().name} - {_SEND_WORKER}]"
    heartbeat_count = 0
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    while True:
        while send_worker_flag.value == 1:
            heartbeat_count += 1
            filtered_local_members = []
            for node_id, (member, entry_time, is_suspicious) in local_membership_list.items():
                if member.address == host:
                    member.heartbeat_counter = heartbeat_count
                    local_membership_list.update({node_id: (member, datetime.now(), is_suspicious)})
                    logger.debug(f"Increased heartbeat counter:\n{pretty_local_membership_list(local_membership_list)}")
                else:
                    filtered_local_members.append(member)
            if len(filtered_local_members) > 0:
                drop_rate_random = random.randint(1, 100)
                if drop_rate_random > _DROP_RATE:
                    total_targets = len(local_membership_list)
                    n_random_targets = min(math.ceil(math.log(total_targets)), total_targets)
                    for random_member in random.sample(filtered_local_members, n_random_targets):
                        (
                            Process(
                                target=send_heartbeat,
                                args=(
                                    server, host,
                                    random_member,
                                    local_membership_list, suspicion_messages_queue,
                                    suspicion_flag
                                )
                            )
                        ).start()
            # Wait t_gossip before sending next heartbeat
            time.sleep(t_gossip)
        if send_worker_flag.value == 1:
            updated_node_version = int(round(datetime.now().timestamp()))
            for node_id, (member, entry_time, is_suspicious) in local_membership_list.items():
                if member.address == host:
                    member_copy = message_pb2.Member()
                    member_copy.CopyFrom(member)
                    member_copy.version_number = updated_node_version
                    local_membership_list.update({node_id: (member_copy, entry_time, is_suspicious)})
                    logger.debug(f"{log_src} Increased version number:\n{pretty_local_membership_list(local_membership_list)}")
