from datetime import datetime
from typing import Dict, Tuple

import message_pb2

LocalMembershipEntry = Tuple[message_pb2.Member, datetime, bool]
LocalMembershipList = Dict[str, LocalMembershipEntry]
