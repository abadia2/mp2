import sys
import unittest

import message_pb2
from receive_worker import _MESSAGE_SIZE

# 10 Virtual Machines and 2 local machines
_MAX_NUM_VMS = 10 + 2


class TestDistributedLogs(unittest.TestCase):
    def test_proto_buf_member(self):
        """ Ensure serialization and deserialization of protobuf messages work as expected """
        m = message_pb2.Member()

        # Test serialization
        m.address = "255.255.255.255"
        m.port = 65535
        m.version_number = sys.maxsize
        m.heartbeat_counter = sys.maxsize
        sml = m.SerializeToString()

        # Test deserialization
        m2 = message_pb2.Member()
        m2.ParseFromString(sml)

        self.assertEqual(m2.address, "255.255.255.255")
        self.assertEqual(m2.port, 65535)
        self.assertEqual(m2.version_number, sys.maxsize)
        self.assertEqual(m2.heartbeat_counter, sys.maxsize)

    def test_proto_buf_member_max_size(self):
        """ Ensure the biggest protobuf member is not bigger than the expected MESSAGE_SIZE """
        m = message_pb2.Member()

        # Biggest IP
        m.address = "255.255.255.255"

        # Largest port
        m.port = 65535

        # Largest allowed version number
        m.version_number = sys.maxsize

        # Largest allowed heartbeat count
        m.heartbeat_counter = sys.maxsize

        # Largest incarnation number
        m.incarnation_number = sys.maxsize

        # The protobuf message in bytes
        sm = m.SerializeToString()

        self.assertLessEqual(len(sm), 51)

    def test_proto_buf_membership_list_max_size(self):
        """ Ensure the biggest protobuf membership list is not bigger than the expected SHARED_MEMORY_SIZE """
        membership_list = message_pb2.MembershipList()

        # Biggest Member in bytes
        member = message_pb2.Member()
        member.address = "255.255.255.255"
        member.port = 65535
        member.version_number = sys.maxsize
        member.heartbeat_counter = sys.maxsize
        member.incarnation_number = sys.maxsize

        for i in range(0, _MAX_NUM_VMS):
            membership_list.members.append(member)

        serialized_membership_list = membership_list.SerializeToString()

        actual_membership_list = message_pb2.MembershipList()
        actual_membership_list.ParseFromString(serialized_membership_list)

        self.assertLessEqual(len(actual_membership_list.SerializeToString()), 636)

    def test_proto_buf_suspicion_messages_list_max_size(self):
        """ Ensure the biggest protobuf membership list is not bigger than the expected SHARED_MEMORY_SIZE """
        suspicion_messages_list = message_pb2.SuspicionMessagesList()

        # Biggest Member in bytes
        member = message_pb2.Member()
        member.address = "255.255.255.255"
        member.port = 65535
        member.version_number = sys.maxsize
        member.heartbeat_counter = sys.maxsize
        member.incarnation_number = sys.maxsize

        # Biggest Member in bytes
        suspicion_message = message_pb2.SuspicionMessage()
        suspicion_message.type = message_pb2.SuspicionType.CONFIRM
        suspicion_message.suspect.CopyFrom(member)

        for i in range(0, _MAX_NUM_VMS):
            suspicion_messages_list.suspicion_messages.append(suspicion_message)

        serialized_membership_list = suspicion_messages_list.SerializeToString()

        deserialized_membership_list = message_pb2.SuspicionMessagesList()
        deserialized_membership_list.ParseFromString(serialized_membership_list)

        self.assertLessEqual(len(deserialized_membership_list.SerializeToString()), 684)

    def test_proto_buf_heartbeat_max_size(self):
        """
        Ensure the biggest heartbeat (with membership list and suspicion messages) is not bigger than MESSAGE_SIZE
        """
        heartbeat = message_pb2.Heartbeat()

        # Biggest Member in bytes
        member = message_pb2.Member()
        member.address = "255.255.255.255"
        member.port = 65535
        member.version_number = sys.maxsize
        member.heartbeat_counter = sys.maxsize
        member.incarnation_number = sys.maxsize

        # Biggest Member in bytes
        suspicion_message = message_pb2.SuspicionMessage()
        suspicion_message.type = message_pb2.SuspicionType.CONFIRM
        suspicion_message.suspect.CopyFrom(member)

        for i in range(0, _MAX_NUM_VMS):
            heartbeat.membership_list.members.append(member)
            heartbeat.suspicion_messages_list.suspicion_messages.append(suspicion_message)

        serialized_message = heartbeat.SerializeToString()
        deserialized_heartbeat = message_pb2.Heartbeat()
        deserialized_heartbeat.ParseFromString(serialized_message)

        self.assertLessEqual(len(deserialized_heartbeat.SerializeToString()), _MESSAGE_SIZE)

    def test_max_proto_buf_byte_deserialization(self):
        """ Ensure the message type can be deserialized """
        test_bytes = (b'\n\xfc\x04\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff\xff\xff\xff\xff\xff\x7f '
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n3\n\x0f255.255.255.255\x10\xff\xff\x03\x18\xff\xff\xff'
                      b'\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\x12\xac\x05\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff'
                      b'\xff\x03\x18\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f\n7\x08\x02\x123\n\x0f255.255.255.255\x10\xff\xff\x03\x18'
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f \xff\xff\xff\xff\xff\xff\xff\xff\x7f('
                      b'\xff\xff\xff\xff\xff\xff\xff\xff\x7f')

        heartbeat = message_pb2.Heartbeat()
        heartbeat.ParseFromString(test_bytes)

        print(heartbeat)


if __name__ == '__main__':
    unittest.main()
