# Gossip and Gossip+Suspicion Implementation

## Overview
Our MP implementation focuses on failure detection using both the Gossip protocol and an enhanced version with suspicion. The core of the system runs from a `main.py` file that orchestrates various "workers" dedicated to different aspects of the protocol. Key processes involved include:

- **Heartbeat Worker (`send_worker.py`)**: Responsible for updating local heartbeat counts and disseminating its membership list to a subset of members. The heartbeat messages are sent via UDP at intervals of `t_gossip` seconds.

- **Suspicion Worker**: When suspicion is enabled, this worker periodically scans the membership list and flags nodes that haven't updated in a while (exceeding `t_fail`). These nodes are labeled as suspicious and are tracked until `t_expiration`. A caching mechanism ensures that duplicate suspicion messages are filtered out.

- **Fail Worker**: This worker directly marks nodes as failed, eliminating the need for a suspicion queue.

- **Receive Worker**: A listener that waits for incoming UDP heartbeat messages. For every received message, a new process is initiated. Depending on whether suspicion is active or not, it either integrates the heartbeat data or inspects the suspicion queue to update node statuses. This worker understands various node states like SUSPICIOUS, ALIVE, and CONFIRM.

- **Admin Worker**: An interactive TCP server that caters to commands from the `admin_client.py` script. Users can toggle suspicion, review the membership list, join or leave the network, and perform other administrative tasks. Notably, when a command like toggling suspicion is received, the change is broadcasted to all concurrently running processes.

## Getting Started

To initiate the server:
```bash
python3 main.py
```

To toggle suspicion, list membership list, and more admin tasks:
```bash
python3 admin_client.py
