import argparse
import subprocess
from multiprocessing import Process

_BASE_DIR = "/cs425"
_PORT = 22
_VMS = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7802.cs.illinois.edu": 2,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}


def fetch_demo_logs(vm_id: str) -> None:
    try:
        ssh = subprocess.Popen(
            ['ssh', '-t', vm_id], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        ssh.stdin.write("nohup python3 /cs425/mp2/receive_worker.py &".encode())
        ssh.stdin.flush()
        ssh.stdin.write("exit".encode())
        ssh.stdin.flush()
    except Exception as e:
        print(f"[{vm_id}] An error occurred: {e}")


def main():
    """ Deploy mp2 to all Virtual Machines, this will not work locally, but only on the Virtual Machines """
    parser = argparse.ArgumentParser(description="Distributed Demo Log Loader")
    parser.add_argument("--vms", type=str, nargs="+", help="List of virtual machine names")
    args = parser.parse_args()
    vms = args.vms or _VMS
    processes = [Process(target=fetch_demo_logs, args=(vm,)) for vm in vms]
    for process in processes:
        process.start()
    for process in processes:
        process.join()


if __name__ == "__main__":
    main()
